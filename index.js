var array = [];
// Ham Them So
function ThemSo() {
  var number = document.getElementById("number").value * 1;
  if (Math.floor(number) != number) {
    alert("Phai nhap vao so nguyen!");
    return;
  }
  array.push(number);
  array.join(", ");
  document.getElementById("array").innerHTML = array;
}

// Ham Tinh tong so duong
function TinhTongSoDuong() {
  var tong = 0;
  array.forEach(function (value) {
    if (value > 0) {
      tong += value;
    }
  });

  document.getElementById("result1").innerHTML = tong;
}

// Ham dem so duong
function DemSoDuong() {
  var count = 0;
  array.forEach(function (value) {
    if (value > 0) {
      count++;
    }
  });
  document.getElementById("result2").innerHTML = `Mảng có ${count} số dương`;
}

// Ham tim so nho nhat
function TimMin() {
  var min = array[0];
  array.forEach(function (value) {
    if (min > value) {
      min = value;
    }
  });
  document.getElementById("result3").innerHTML = `Min: ${min}`;
}

// Ham tim so duong nho nhat
function TimMinDuong() {
  var array_duong = [];
  array.forEach(function (value) {
    if (value > 0) {
      array_duong.push(value);
    }
  });
  min_duong = array_duong[0];
  array_duong.forEach(function (value) {
    if (min_duong > value) {
      min_duong = value;
    }
  });
  document.getElementById(
    "result4"
  ).innerHTML = `So duong nho nhat: ${min_duong}`;
}

// Ham tim so chan cuoi
function TimSoChanCuoi() {
  var sochan = [];
  array.forEach(function (value) {
    if (value % 2 == 0) {
      sochan.push(value);
    }
  });
  if (sochan.length == 0) {
    document.getElementById("result5").innerHTML = `-1`;
  } else {
    var sochancuoi = sochan.pop();
    document.getElementById(
      "result5"
    ).innerHTML = `So chan cuoi: ${sochancuoi}`;
  }
}

// Ham doi cho
function DoiCho() {
  var index1 = document.getElementById("index1").value;
  var index2 = document.getElementById("index2").value;
  var temp = array[index1];
  array[index1] = array[index2];
  array[index2] = temp;
  document.getElementById("result6").innerHTML = array;
}

// Ham sap xep
function Sapxep() {
  array.sort();
  document.getElementById("result7").innerHTML = array;
}

// Ham tim SNT dau tien
function TimSNT() {
  var SNT = [];
  array.forEach(function (value) {
    if (value == 2) {
      SNT.push(2);
    }
    var condition = 1;
    for (var i = 2; i <= Math.sqrt(value); i++) {
      if (value % i == 0) {
        condition++;
      }
    }
    if (condition == 1 && value > 2) {
      SNT.push(value);
    }
  });
  if (SNT.length == 0) {
    document.getElementById("result8").innerHTML = `-1`;
  } else {
    document.getElementById(
      "result8"
    ).innerHTML = `So nguyen to dau tien: ${SNT[0]}`;
  }
  console.log(SNT);
}

// Ham dem so nguyen
var array2 = [];
function Them() {
  var input = document.getElementById("numbers").value * 1;
  array2.push(input);
  document.getElementById("array2").innerHTML = array2;
}

function DemSoNguyen() {
  var countInteger = 0;
  array2.forEach(function (value) {
    if (Number.isInteger(value) == true) {
      countInteger++;
    }
  });
  document.getElementById("result9").innerHTML = `So nguyen: ${countInteger}`;
}

// So sanh so luong duong va am
function SoSanhAmDuong() {
  var soAm = 0;
  var soDuong = 0;
  array.forEach(function (value) {
    if (value > 0) {
      soDuong++;
    } else if (value < 0) {
      soAm++;
    }
  });
  if (soAm > soDuong) {
    document.getElementById("result10").innerHTML = `So am > So duong`;
  } else if (soAm < soDuong) {
    document.getElementById("result10").innerHTML = `So am < So duong`;
  } else {
    document.getElementById("result10").innerHTML = `So am = So duong`;
  }
}
